// ImageChatPredictor.js

const fetch = require('node-fetch');
const FormData = require('form-data');
const fs = require('fs');

class ImageChatPredictor {
    constructor(apiKey, modelId, hostName) {
        this.apiKey = apiKey;
        this.modelId = modelId;
        this.hostName = hostName || "https://chat-api.chooch.ai";
    }

    createFormData(parameters, fileBuffer) {
        const formData = new FormData();
        formData.append('data', JSON.stringify({ parameters, model_id: this.modelId }));
        formData.append('file', fileBuffer, { filename: 'upload.jpg' });
        return formData;
    }

    async performRequest(formData) {
        const url = `${this.hostName}/predict?api_key=${this.apiKey}`;
        const response = await fetch(url, {
            method: 'POST',
            body: formData,
            headers: formData.getHeaders()
        });
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
        return await response.json();
    }

    async predict(parameters, fileBuffer) {
        const formData = this.createFormData(parameters, fileBuffer);
        try {
            return await this.performRequest(formData);
        } catch (error) {
            return error;
        }
    }
}

module.exports = ImageChatPredictor;
