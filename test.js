const fs = require('fs/promises');
const ImageChatPredictor = require('./index.js');

// Example usage
(async () => {
    const apiKey = process.argv[2] || 'xxxxxxxxx-xxxxx-xxxxxxx-xxxxxxx';
    const modelId = "chooch-image-chat-3";
    const predictor = new ImageChatPredictor(apiKey, modelId);
    const parameters = {
        prompt: "Is someone concealing an item? Yes or No."
    };
    const fileBuffer = await fs.readFile("files/1.jpg");
    const returnVal = await predictor.predict(parameters, fileBuffer);
    console.log(returnVal);
})();
