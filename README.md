# node-chooch

Unofficial Node.js wrapper for Chooch.com

Currently only providing API endpoints we use in our own application.

We will not take requests to update this module unless directly requested by Chooch.com.
